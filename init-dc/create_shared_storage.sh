#!/bin/bash

# Check NFS installation or install if needed
echo "Creating shared storage ..."
echo "Checking NFS-server installation ..."
if [ ! -e /etc/init.d/nfs-kernel-server ]; then
    sudo apt update
    echo "NFS server is being installed ..."
    sudo apt install -y \
        ntp \
        nfs-common \
        nfs-kernel-server
    sudo apt autoremove -y
    echo "NFS server and essentials were installed."
else
    echo "NFS server and essentials are already installed."
fi

# Create directories in case the installation is DevStack
echo "Check existence of Nova directory ..."
if [ ! -e /var/lib/nova ]; then
    sudo mkdir /var/lib/nova
    echo "Nova directory did not exist so it was created."
    sudo ln -s /opt/stack/data/nova/instances /var/lib/nova
    echo "Create symbolic link to /opt/stack/data/nova/instances."
    sudo chmod o+x /opt/stack/data/nova/instances
    sudo chown -R stack:root /opt/stack/data/nova/instances
    sudo chown -R stack:root /var/lib/nova/instances
    echo "Passed ownership of instances to stack:root."
fi
sudo mkdir /opt/export
sudo mkdir /opt/export/instances
sudo chmod o+x /var/lib/nova/instances
sudo chmod o+x /opt/export/instances
sudo chown -R stack:root /opt/export/instances

# Export instances & restart NFS
echo "/opt/export/instances *(rw,insecure,no_subtree_check,async,no_root_squash)" | sudo tee -a /etc/exports
echo "127.0.0.1:/opt/export/instances /opt/stack/data/nova/instances nfs4 defaults 0 0" | sudo tee -a /etc/fstab
sudo service nfs-server restart
echo "Shared storage has been created."
sudo mount -a -v
echo "Shared storage has been mounted."
