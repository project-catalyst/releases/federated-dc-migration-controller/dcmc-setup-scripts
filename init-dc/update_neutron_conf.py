import configparser

config = configparser.ConfigParser()
config.read('/etc/neutron/plugins/ml2/ml2_conf.ini')

config['ml2']['mechanism_drivers'] = 'openvswitch,linuxbridge,l2population'
config['agent']['l2_population'] = 'True'
config['agent']['arp_responder'] = 'True'

with open('/etc/neutron/plugins/ml2/ml2_conf.ini', 'w') as neutron_conf:
    config.write(neutron_conf)
