#!/usr/bin/env bash:

# Export essentials
# export OS_USERNAME=
# export OS_PASSWORD=
# export OS_PROJECT_NAME=
# export OS_USER_DOMAIN_NAME=
# export OS_PROJECT_DOMAIN_NAME=
# export OS_AUTH_URL=

# Or source from openrc
source /opt/stack/devstack/openrc admin

echo "Initializing Federation DC running on host '`hostname`'..."
echo "Active user is '${OS_USERNAME}', active project is '${OS_PROJECT_NAME}'."
echo "User domain is '${OS_USER_DOMAIN_NAME}', current project domain is '${OS_PROJECT_DOMAIN_NAME}'."
echo "Authorization URL has been set to '${OS_AUTH_URL}'."

# Create Catalyst Project
openstack project create --description 'H2020 Catalyst' Catalyst --domain default
echo "Created Project 'Catalyst'."
openstack role add --user admin --project Catalyst member
openstack role add --user admin --project Catalyst admin
echo "Added user 'admin' to project 'Catalyst' as member, admin."
openstack role add --group admins --project Catalyst member
openstack role add --group admins --project Catalyst admin
echo "Added group 'admins' to project 'Catalyst' as member, admin."

# Export Project Catalyst
export OS_PROJECT_NAME=Catalyst
echo "Active user is '${OS_USERNAME}', active project is '${OS_PROJECT_NAME}'."

# Create Catalyst Network & Router
openstack network create catalyst
echo "Created network 'catalyst'."
openstack subnet create catalyst-subnet --network catalyst --subnet-range 10.0.0.0/24
echo "Created subnet 'catalyst-subnet' for network 'catalyst' with range '10.0.0.0/24'."
openstack router create catalyst-router
echo "Created router 'catalyst-router'."
openstack router set catalyst-router --external-gateway public
echo "Set external gateway to 'public' for 'catalyst-router'."
openstack router add subnet catalyst-router catalyst-subnet
echo "Added subnet 'catalyst-subnet' to 'catalyst-router'."

# Add security group and rules
openstack security group create catalyst-sec-group --description 'H2020 Catalyst'
echo "Created security group 'catalyst-sec-group'."
openstack security group rule create catalyst-sec-group --protocol tcp --dst-port 22:22 --remote-ip 0.0.0.0/0
echo "Added INGRESS TCP RULE for port 22 (SSH) to security group 'catalyst-sec-grpup'."
openstack security group rule create catalyst-sec-group --protocol tcp --dst-port 80:80 --remote-ip 0.0.0.0/0
echo "Added INGRESS TCP RULE for port 80 (HTTP) to security group 'catalyst-sec-group'."
openstack security group rule create catalyst-sec-group --protocol tcp --dst-port 1:65535 --remote-ip 0.0.0.0/0
echo "Added INGRESS ALL TCP RULE to security group 'catalyst-sec-group'."
openstack security group rule create catalyst-sec-group --protocol udp --dst-port 1:65535 --remote-ip 0.0.0.0/0
echo "Added INGRESS ALL UDP RULE to security group 'catalyst-sec-group'."
openstack security group rule create catalyst-sec-group --protocol icmp --remote-ip 0.0.0.0/0
echo "Added ICMP rule to security group 'catalyst-sec-group'."

# Create keypair
openstack keypair create --private-key catalyst.pem catalyst
echo "Created keypair 'catalyst'. It can be found in the current directory."

# Create vCMP image, assuming the file /opt/stack/vCompute-Train-v1.0.0-SNAPSHOT.img exists
openstack image create --disk-format qcow2 --container-format bare \
    --public --file /opt/stack/vCompute-Train-v1.0.0-SNAPSHOT.img vCompute-Train-v1.0.0-SNAPSHOT
echo "Created image 'vCompute-Train-v1.0.0-SHAPSHOT'."

# Create migration test VM
echo "Creating VM instance for migration testing."
openstack server create --flavor m1.nano --image cirros-0.4.0-x86_64-disk \
    --nic net-id=$(openstack network list | grep catalyst | sed 's,| ,,' | awk -F '|' '{print $1}') \
    --security-group catalyst-sec-group --key-name catalyst migration_test
echo "Created VM instance 'migration_test' with flavor 'm1.nano' and image 'cirros-0.4.0-x86_64-disk'."

# Update Nova & Neutron configuration files
echo "Updating Nova configuration"
sudo python3 update_nova_conf.py
echo "Updating Neutron configuration"
sudo python3 update_neutron_conf.py

# Restart Devstack
sudo systemctl restart devstack@*

# Give internet access to VMs even if they are not in the public network
sudo iptables -t nat -A POSTROUTING -o br-ex -j MASQUERADE

