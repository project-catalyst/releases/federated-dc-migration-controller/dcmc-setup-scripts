import configparser

config = configparser.ConfigParser()
config.read('/etc/nova/nova.conf')

config['scheduler']['discover_hosts_in_cells_interval'] = '15'
config['libvirt']['cpu_mode'] = 'host-passthrough'

with open('/etc/nova/nova.conf', 'w') as nova_conf:
    config.write(nova_conf)
