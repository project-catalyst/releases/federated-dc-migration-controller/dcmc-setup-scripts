#!/bin/bash

cd init-dc

echo "Ready to execute init_federation_dc.sh ..."
./init_federation_dc.sh
echo "Script completed."

echo "Ready to execute create_shared_storage.sh ..."
./create_shared_storage.sh
echo "Script completed."

echo "Ready to execute upgrade_to_train.sh ..."
./upgrade_to_train.sh
echo "Script completed."
echo "Federation DC has been prepared."

