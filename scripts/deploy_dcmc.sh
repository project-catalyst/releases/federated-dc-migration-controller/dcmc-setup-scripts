#!/usr/bin/env bash

# Handle different installation types
if [ "${1}" == "all_in_one" ]; then
    echo "Deploying DCMC Master Client ..."
    docker-compose -f ./dcmc/master/docker-compose.yml --project-directory ./dcmc/master/ up -d
    echo "DCMC Master Client was deployed."
    echo "Deploying DCMC Lite Client ..."
    docker-compose -f ./dcmc/lite/docker-compose.yml --project-directory ./dcmc/lite/ up -d
    echo "DCMC Lite Client was deployed."
    echo "Deploying ITL Marketplace Connector ..."
    docker-compose -f ./dcmc/itl-connector/docker-compose.yml --project-directory ./dcmc/itl-connector/ up -d
    echo "ITL Marketplace Connector was deployed."
    echo "Pulling OpenVPN Client image ..."
    docker pull powerops/catalyst-openvpn-client
    docker tag powerops/catalyst-openvpn-client catalyst-openvpn-client
elif [ "${1}" == "controller_only" ]; then
    echo "Deploying DCMC Master Client ..."
    docker-compose -f ./dcmc/master/docker-compose.yml --project-directory ./dcmc/master/ up -d
    echo "DCMC Master Client was deployed."
    echo "Deploying ITL Marketplace Connector ..."
    docker-compose -f ./dcmc/itl-connector/docker-compose.yml --project-directory ./dcmc/itl-connector/ up -d
    echo "ITL Marketplace Connector was deployed."
    echo "Pulling OpenVPN Client image"
    docker pull powerops/catalyst-openvpn-client
    docker tag powerops/catalyst-openvpn-client catalyst-openvpn-client
else
    docker-compose -f ./dcmc/${1}/docker-compose.yml --project-directory ./dcmc/${1}/ up -d
fi
