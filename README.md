# DCMC Setup Scripts

This project includes scripts targeted to the preparation of a DC to be part of the H2020 CATALYST Federation. When a DC becomes a member of the CATALYST Federation,
it will be able to live migrate IT Load to other DCs, members of the Federation. The setup is divided in two parts:

  * the essential preparation of the DC;
  * the deployment of the DC Migration Controller (DCMC)

In the following, the scripts for the preparation of the DC, as well as the deployment of the DCMC components are described. The structure of the repository appears in the
following tree.

```
.
├── dcmc
│   ├── itl-connector
│   │   ├── docker-compose.yml
│   │   └── .env
│   ├── lite
│   │   ├── docker-compose.yml
│   │   └── .env
│   ├── master
│   │   ├── docker-compose.yml
│   │   ├── .env
│   │   └── nginx
│   │       ├── Dockerfile
│   │       └── nginx.conf
│   └── server
│       ├── docker-compose.yml
│       ├── .env
│       └── nginx
│           ├── Dockerfile
│           └── nginx.conf
├── init-dc
│   ├── create_shared_storage.sh
│   ├── init_federation_dc.sh
│   ├── update_neutron_conf.py
│   ├── update_nova_conf.py
│   └── upgrade_essentials.sh
└── scripts
    ├── deploy_dcmc.sh
    └── prepare_dc.sh

```

We should notice that this repository includes four branches (besides the master) including DC-specific settings for the deployment in POPs (Laptop & Old Controller), QRN and PSNC.

## DC Preparation

The preparation of a DC includes the following steps:

  * The initialization of Catalyst essentials, composed by the following steps, executed by the script *init_federation_dc.sh*:
    * the creation of a Catalyst project and assignment of roles;
    * the initialization of the Catalyst networking (network, subnet, router);
    * the creation / uploading of the virtual Compute (vCMP) image;
    * the creation of a VM to be used as a migration test.
  * The creation of shared storage, utilizing NFS, if non-existent, executed by the script *create_shared_storage.sh*:
    * installation of ntp, nfs-common & nfs-kernel-server;
    * creation of symbolic links in case of DevStack installation;
    * export of instances through the /opt/export/instances directory.
  * The upgrade of OpenStack components to Train version (https://docs.openstack.org/train/), if older versions are installed, executed by the script *upgrade_to_train.sh*:
    * addition of the Train repository;
    * performing a dist-upgrade;
    * ensure the installed version of Libvirt is at least v4.0.0.

To execute all of the above steps, you may run the script *prepare_dc.sh*:

```bash
./scripts/prepare_dc.sh
```

If one or more steps need to be ommited, one may execute the necessary scripts, lying in the *init-dc* directory selectively.

## Deployment of DCMC Components

In this section the docker-compose and other prerequisite files for the deployment of the DCMC services are described. The user can either install each component separately 
or install them altogether. Each of the present directories includes a .env file. This file should be updated accordingly to the specifics of each deployment site. The
services that can be installed through the *scripts/deploy_dcmc.sh* script include:

  * IT Load Marketplace Connector
  * DCMC Master Client
  * DCMC Lite Client
  * DCMC Server

The following options can be passed as arguments to *deploy_dcmc.sh*:

| Argument | Components Installed | When to use | Host Type |
| -------- | -------------------- | ----------- | ------------ |
| __all_in_one__ | `ITL Marketplace Connector` `DCMC Master` `DCMC Lite` | All-in-one deployments, where the controller and compute nodes co-exist in the same machine. | Controller |
| __controller_only__ | `ITL Marketplace Connector` `DCMC Master` | Distributed deployments, where multiple compute nodes are deployed in different hosts from the controller. | Controller |
| __itl-connector__ | `ITL Marketplace Connector` | When only ITL Marketplace Connector should be installed | Controller |
| __lite__ | `DCMC Lite Client` | When only DCMC Lite Client should be installed | Compute |
| __master__ | `DCMC Master Client` | When only DCMC Master client should be installed | Controller |
| __server__ | `DCMC Server` | When deployment of the Federation's DCMC Server is required  | Federation |

For example, for an all-in-one OpenStack deployment, a case where all controller components should be installed, the following command can be executed:

```bash
./scripts/deploy_dcmc.sh all_in_one
```

In the following sections, the environment variables that must be set in each of the .env files are described for each component.

### ITL Marketplace Connector

__Docker__

| Environment Variable | Tag | Description |
| -------------------- | --- | ----------- |
| COMPOSE_PROJECT_NAME | `docker` | The name of the project (default: catalyst_itl_connector) |

__External Services__

| Environment Variable | Tag | Description |
| -------------------- | --- | ----------- |
| MARKETPLACE_USERNAME | `Marketplace` | Username |
| MARKETPLACE_PASSWORD | `Marketplace` | Password |
| MARKETPLACE_IB_BASE_URL | `Marketplace` | Host Base URL |
| DCMCM_USERNAME | `DCMC Server` | Username |
| DCMCM_PASSWORD | `DCMC Server` | Password |
| DCMCM_URL | `DCMC Server` | Host URL |
| OAUTH_CLIENT_ID | `Keycloak` | Client ID |
| OAUTH_CLIENT_SECRET | `Keycloak` | Client Secret |
| OAUTH_REALM | `Keycloak` | Realm |
| OAUTH_URL | `Keycloak` | Host URL |
| OAUTH_USER_TOKEN | `Keycloak` | Open ID Token URL |


### DCMC Lite Client

__Docker and Internal Settings__

| Environment Variable | Tag | Description |
| -------------------- | --- | ----------- |
| COMPOSE_PROJECT_NAME | `docker` | The project name (default: catalyst) |
| DCMCL_DC_HOST_TYPE | `DCMC Lite` | The host type in the form <deployment_type>:<host_type>. Possible values: *deployment_type*: {all_in_one, distributed}, *host_type*: {physical, virtual} |
| DCMCL_PHYSICAL_NI | `DCMC Lite` | The machine's physical network interface |
| DCMCL_SSH_KEY_DIR | `DCMC Lite` | SSH directory to mount for SSH-key exchange (e.g. /opt/stack/.ssh)  |
| DCMCL_TUN_DEVICE | `DCMC Lite` | Temporary settings, until each transaction is mapped to a tun interface  |


__External Services__

| Environment Variable | Tag | Description |
| -------------------- | --- | ----------- |
| COMPOSE_PROJECT_NAME | `docker` | The project name (default: catalyst) |
| DCMCL_KEYCLOAK_USERNAME | `Keycloak` | Username |
| DCMCL_KEYCLOAK_PASSWORD | `Keycloak` | Password |
| DCMCL_COVPN_HOST_URL | `Cloud OpenVPN` | Host URL |
| DCMCL_COVPN_OWNER |`Cloud OpenVPN` | Connection Owner |
| DCMCL_COVPN_TENANT |`Cloud OpenVPN` | Tenant (default: Catalyst) |
| DCMCL_DCMCS_HOST_URL | `DCMC Server` | Host URL |

### DCMC Master Client

__Docker and Internal Services__

| Environment Variable | Tag | Description |
| -------------------- | --- | ----------- |
| COMPOSE_PROJECT_NAME | `docker` | The name of the project (default: catalyst) |
| PG_IMAGE_TAG | `PostgreSQL` | Docker Image Tag |
| PG_PORT | `PostgreSQL` | Port |
| PG_USER | `PostgreSQL` | User |
| PG_PASSWORD | `PostgreSQL` | DB Password |
| PG_DB | `PostgreSQL` | DB Name |
| REDIS_IMAGE_TAG | `Redis` | Docker Image Tag |
| REDIS_PORT | `Redis` | Port |
| DCMCM_ENV | `DCMC Master` | Environment (dev / prod) |
| DCMCM_API_PORT | `DCMC Master` | API Port |
| DCMCM_SUPERVISOR_PORT | `DCMC Master` | Supervisor Port |
| DCMCM_DB_HOST | `DCMC Master` | DB Host |
| DCMCM_DB_PORT | `DCMC Master` | DB Port |
| DCMCM_DB_USER | `DCMC Master` | DB User |
| DCMCM_DB_PASSWORD | `DCMC Master` | DB Password |
| DCMCM_DB_NAME | `DCMC Master` | DB Name |
| DCMCM_REDIS_HOST | `DCMC Master` | Redis Host |
| DCMCM_REDIS_PORT | `DCMC Master` | Redis Port |
| DCMCM_HOST_IP | `DCMC Master` | Host IP |
| DCMCM_HOST_PORT | `DCMC Master` | Host Port |
| DCMCM_HOST_PROTOCOL | `DCMC Master` | Host Protocol |
| DCMCM_DC_TYPE=openstack | `DCMC Master` | DC Type |


__OpenStack Services & Credentials__

| Environment Variable | Tag | Description |
| -------------------- | --- | ----------- |
| DCMCM_OS_DC_NAME | `OpenStack` | DC Name  |
| DCMCM_OS_AUTH_URL | `OpenStack` | Authorization |
| DCMCM_OS_CONTROLLER | `OpenStack` | Controller IP |
| DCMCM_OS_CONTROLLER_USER | `OpenStack` | Controller's Host Machine User |
| DCMCM_OS_CONTROLLER_PWD | `OpenStack` | Controller's Host Machine Pwd |
| DCMCM_OS_USERNAME | `OpenStack` | Username |
| DCMCM_OS_USER_PWD | `OpenStack` | Password |
| DCMCM_OS_PROJECT_NAME | `OpenStack` | Project Name |
| DCMCM_OS_PROJECT_DOMAIN_ID | `OpenStack` | Project domain (default: default) |
| DCMCM_OS_USER_DOMAIN_ID | `OpenStack` | User Domain (default: default) |
| DCMCM_OS_TRANSPORT_USER | `RabbitMQ` | Transport User |
| DCMCM_OS_TRANSPORT_PASSWORD | `RabbitMQ` | Transport Password |
| DCMCM_OS_TRANSPORT_IP | `RabbitMQ` | Transport IP |
| DCMCM_OS_TRANSPORT_PORT | `RabbitMQ` | Transport Port |
| DCMCM_OS_AUTHENTICATE_URI | `Keystone` | Service Endpoint |
| DCMCM_OS_AUTHORIZATION_URL | `Keystone` | Service Endpoint |
| DCMCM_OS_GLANCE_SERVICE | `Glance` | Service Endpoint |
| DCMCM_OS_MEMCACHED_SERVICE | `Memcached` | Service Host |
| DCMCM_OS_MEMCACHED_SECRET_KEY | `Memcached` | Secret Key |
| DCMCM_OS_NEUTRON_SERVICE | `Neutron` | Service Endpoint |
| DCMCM_OS_NEUTRON_PASS | `Neutron` | Password |
| DCMCM_OS_NOVA_PASS | `Nova` | Password |
| DCMCM_OS_PLACEMENT_SERVICE | `Placement` | Service Endpoint |
| DCMCM_OS_PLACEMENT_PASS | `Placement` | Password |
| DCMCM_OS_TRANSPORT_URL | `RabbitMQ` | Transport URL |
| DCMCM_OS_NFS_SERVER_HOST | `NFS` | Host IP |
| DCMCM_OS_NFS_SERVER_ROOT | `NFS` | Exposed Directory |
| DCMCM_OS_NOVA_UID | `Nova` | UID (nova) |
| DCMCM_OS_NOVA_GID | `Nova` | GID (nova) |
| DCMCM_OS_LIBVIRT_UID | `Libvirt` `QEMU` | UID (libvirt-qemu) |
| DCMCM_OS_LIBVIRT_GID | `Libvirt` `QEMU` | GID (kvm) |
| DCMCM_OS_LIBVIRT_GROUP | `Libvirt` | GID (libvirt) |
| DCMCM_OS_LIBVIRT_USER | `Libvirt` | User for Libvirt |
| DCMCM_OS_VCMP_IMAGE | `vCMP` | Image Name (default: vCompute-Train-v0.9.8-SNAPSHOT) |
| DCMCM_OS_VCMP_PROJECT | `vCMP` | Project Name (default: Catalyst)|
| DCMCM_OS_VCMP_KEYPAIR | `vCMP` | Keypair (default: none) |
| DCMCM_OS_VCMP_MIN_CPU | `vCMP` | Minimum numbers or CPUs (default: 1) |
| DCMCM_OS_VCMP_MIN_RAM | `vCMP` | Minimum amount of RAM (MB) (default: 2048) |
| DCMCM_OS_VCMP_MIN_DISK | `vCMP` | Minimum amount of disk (GB) (default: 20) |


__External Services__

| Environment Variable | Tag | Description |
| -------------------- | --- | ----------- |
| DCMCM_VCG_IP | `VCG` | Host IP |
| DCMCM_VCG_PORT | `VCG` | Port |
| DCMCM_VCG_PROTOCOL | `VCG` | Protocol |
| DCMCM_ITLB_IP | `IT Load Balancer` | Host IP |
| DCMCM_ITLB_PORT | `IT Load Balancer` | Port |
| DCMCM_ITLB_PROTOCOL | `IT Load Balancer` | Protocol |
| DCMCM_ITLMC_IP | `IT Load Markerplace Connector` | Host IP |
| DCMCM_ITLMC_PORT | `IT Load Markerplace Connector` | Port |
| DCMCM_ITLMC_PROTOCOL | `IT Load Markerplace Connector` | Protocol |
| DCMCM_DCMCS_IP | `DCMC Server` | Host IP |
| DCMCM_DCMCS_PORT | `DCMC Server` | Port |
| DCMCM_DCMCS_PROTOCOL | `DCMC Server` | Protocol |
| DCMCM_KEYCLOAK_URL | `Keycloak` | Host URL |
| DCMCM_KEYCLOAK_REALM | `Keycloak` | Realm |
| DCMCM_KEYCLOAK_CLIENT_ID | `Keycloak` | Client ID |
| DCMCM_KEYCLOAK_CLIENT_SECRET | `Keycloak` | Client Secret |
| DCMCM_KEYCLOAK_USERNAME | `Keycloak` | Username |
| DCMCM_KEYCLOAK_PASSWORD | `Keycloak` | Password |
| DCMCM_COVPN_HOST_URL | `Cloud OpenVPN` | Host URL |
| DCMCM_COVPN_TENANT | `Cloud OpenVPN` | Tenant |
| DCMCM_COVPN_PORT | `Cloud OpenVPN` | Connection Port |
| DCMCM_DCMCL_DEFAULT_PORT | `DCMC Lite` | Default Port (default: 60004) |

__Temporary Settings__

| Environment Variable | Description |
| -------------------- | ----------- |
| DCMCM_VC_UUID | UUID of the VM for migration testing. Temporarily static, until VCG is running as well. |
| DCMCM_TUN_DEVICE | Tun device (e.g. tun0) to be used. Temporary, as each tun device will be mapped to a transaction. |


### DCMC Server

__Docker and Internal Services__

| Parameter | Tag | Description |
| --------- | --- | ----------- |
| COMPOSE_PROJECT_NAME | `docker` | The name of the project (default: catalyst) |
| PG_IMAGE_TAG | `PostgreSQL` | The Postgresql Docker Image tag |
| PG_PORT | `PostgreSQL` | Port |
| PG_USER | `PostgreSQL` | User |
| PG_PASSWORD | `PostgreSQL` | Password |
| PG_DB | `PostgreSQL` | DB Name |
| REDIS_IMAGE_TAG | `Redis` | Docker Image tag |
| REDIS_PORT | `Redis` | Port |
| DCMCS_ENV | `DCMC Server` | Environment (dev / prod) |
| DCMCS_API_PORT | `DCMC Server` | API Port |
| DCMCS_SUPERVISOR_PORT | `DCMC Server` | Supervisor Port |
| DCMCS_DB_HOST | `DCMC Server` | DB Host |
| DCMCS_DB_PORT | `DCMC Server` | DB Port |
| DCMCS_DB_USER | `DCMC Server` | DB User |
| DCMCS_DB_PASSWORD | `DCMC Server` | DB Password |
| DCMCS_DB_NAME | `DCMC Server` | DB Name |
| DCMCS_REDIS_HOST | `DCMC Server` | Redis Host |
| DCMCS_REDIS_PORT | `DCMC Server` | Redis Port |
| DCMCS_HOST_IP | `DCMC Server` | Host IP |
| DCMCS_HOST_PORT | `DCMC Server` | Port |
| DCMCS_HOST_PROTOCOL | `DCMC Server` | Protocol |

__External services__

| Parameter | Tag | Description |
| --------- | --- | ----------- |
| DCMCS_KEYCLOAK_URL | `Keycloak` | Host URL |
| DCMCS_KEYCLOAK_REALM | `Keycloak` | Realm |
| DCMCS_KEYCLOAK_CLIENT_ID | `Keycloak` | Client ID |
| DCMCS_KEYCLOAK_CLIENT_SECRET | `Keycloak` | Client Secret |

